<div class="row">
  <div class="col-md-12">
  	<h1><span class="glyphicon glyphicon-star-empty"></span> Sobre</h1>
  	<hr />
  	<div class="col-md-12 text-center">
    <img class="img" src="<?php echo base_url('assets/img/nick.jpeg'); ?>" alt="Nicolas Medeiro" width="140" height="140">
      <h2><span class="glyphicon glyphicon-star-empty"></span> Nicolas Charles Oliveira Medeiro <span class="glyphicon glyphicon-star-empty"></span></h2>
      <p>Graduado em Ciência da Computação pela UTFPR, mestrando no programa de Pós Graduação em Ciência da Computação pela UTFPR, possui experiencia em desenvolvimento web.</p>
      <p>WhatsApp: +55 42 99961-8959</p>
      <p>E-mail: medeiro@alunos.utfpr.edu.br</p>
      <p><a href="https://gitlab.com/nickmedeiro" title="GitLab" target="_blank">GitLab</a></p>
      <p><a href="http://lattes.cnpq.br/7632722093314056" title="Currículo Lattes" target="_blank">Currículo Lattes</a></p>
      <p><a class="btn btn-default" href="https://www.facebook.com/nickoooliveira" role="button" target="_blank">Facebook »</a></p>
    </div>
  </div>
</div>