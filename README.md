# Bootstrap e CodeIgniter: Desenvolvimento de uma pequena agenda

Este pequeno código é uma demonstração de habilidades para desenvolvimento web.

### Objetivo
Criação de um CRUD para armazenar nome, email e telefone.

### Requisitos técnicos utilizados:
  - HTML / CSS;
  - Linguagem JavaScript;
  - Linguagem PHP; e
  - Linguagem SQL.

### Softwares Utilizados:
- XAMPP;
- GIT;
- SGBD MySQL; 
- CodeIgniter 3.0.2;
- Bootstrap 3.3.5;
- IDE: Visual Studio Code;
- Versionamento de código: GitLab

### Como executar o projeto localmente:
É necessário possuir instalado em sua máquina o XAMPP para rodar o apache (para funcionar a codificação PHP) e também o _SGBD MySQL_ (para usar o banco de dados).

Por inicio, deve-se clonar o código _(git clone https://gitlab.com/nickmedeiro/crud-mm.git)_ dentro da pasta _htdocs_, no XAMPP. Esse passo pode ser executado dentro de uma IDE ou via git diretamente no diretório mencionado.

Após a instalação do _SGBD MySQL_, deve-se rodar o _script_ de criação do banco de dados; o mesmo encontra-se na pasta raíz do projeto e chama-se bd_mvc_crud.sql. Faz-se necessário alterar os parâmetros de acesso ao BD em  __application/config/database.php__

```php
$db['default'] = array(
	'dsn'	=> '',
	'hostname' => 'localhost',
	'username' => 'root',
	'password' => 'teste',
	'database' => 'mvc_crud',
	'dbdriver' => 'mysqli',
	...
```
No caso acima, o BD está localmente na máquina, com user _root_ e password _teste_, devendo ser alterados somente se for mudar os parâmetros de login.  

Por fim, é necessário iniciar o XAMPP e ativar o módulo _apache_, liberando o acesso local ao link _(http://localhost/crud/)_ e ao projeto funcionando.

